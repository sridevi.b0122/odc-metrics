import React from 'react';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import Home from './components/home/home';
import { Provider } from "react-redux";
import history from './appHistory';
import {store } from "./redux/store";
const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;
function App() {
  return (
    <div className="">
      <Provider store={store}>
        <Router history={history}>
          <React.Suspense fallback={loading()}>
            <Switch>
              <Switch>
                <Route path="/" name="Home" render={props => <Home {...props} />} />
              </Switch>
            </Switch>
          </React.Suspense>
        </Router>
      </Provider>

    </div>
  );
}

export default App;
