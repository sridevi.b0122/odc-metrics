import React, { Component } from 'react';
import Dashboard from '../dashboard/dashboard';
import Header from '../header/header';
import Footer from '../footer/footer';
import { Switch, withRouter, Route, BrowserRouter as Router } from 'react-router-dom';
import UploadPage from '../upload/upload';
import Sidebar from '../sidebar/sidebar'
import MetricDetails from '../metric-details/metric-details';
import '../sidebar/sidebar.css'
import Drawer from 'rc-drawer';
import 'rc-drawer/assets/index.css';
const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;
class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showNavbar: false,open:false
        }
        this.onSwitch = this.onSwitch.bind(this);
    }
   
     onSwitch = () => {
        const { open } = this.state;
        this.setState({
          open: !open,
        });
      }
       onChange = (bool) => {
        console.log('change: ', bool);
      }
      onTouchEnd = () => {
        this.setState({
          open: false,
        });
      }
    render() {
        console.log(this.state.showNavbar)
        return (
            <div className="container-fluid">
                <Router>
                <div className="row">  
                    <Drawer
                        placement="left" 
                        mode="inline"
                        handler={false}
                        open={this.state.open}
                        onChange={this.onChange}
                        onClose={this.onTouchEnd}
                    >
                        <Sidebar toggleNav={this.onSwitch}/>
                    </Drawer>
                 
                    <div className={"col-md-12 transition p-0 "}>
                        <Header toggleNav={this.onSwitch}/>
                    </div>
                    <div className="col-md-12">
                            <div className="row">
                                <div id="main" className={"col-md-12 transition "}>
                                    <React.Suspense fallback={loading()}>
                                        <Switch>
                                            <Route path="/" name="dashboard" exact={true} render={props => <Dashboard {...props} />} />
                                            <Route path="/upload" exact={true} name="upload" render={props => <UploadPage {...props} />} />
                                            <Route path="/metric-details" exact={true} name="upload" render={props => <MetricDetails {...props} />} />
                                        </Switch>
                                    </React.Suspense>
                                </div>
                            </div>
                    </div>
                </div>
                        </Router>
                <Footer />
            </div>
        )
    }
}
export default Home;