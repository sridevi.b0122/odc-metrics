import React,{Component} from 'react';

class Header extends Component {
    render(){
        return(
            <div className="header">
             <div className="">
             <button class="btn btn-sm text-white" onClick={this.props.toggleNav}><span><i class="fas fa-bars"></i></span></button>
             ODC METRICS
             </div>
            </div> 
        )
    } 
}
export default Header; 