import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom'
class Sidebar extends Component {
    constructor(props){
        super(props)

    }
    onClickLink(link){
        this.props.toggleNav();
        this.props.history.push(link)

    }
    render() {
        return ( 
            <div className="text-center" style={{width: "100px",fontSize:"25px" }}>
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <span className="text-dark cursor-pointer" onClick={()=>this.onClickLink('/')}><i class="fas fa-home"></i></span>
                    </li>
                    <li class="nav-item">
                        <span className="text-dark cursor-pointer" onClick={()=>this.onClickLink('/upload')}><i class="fas fa-cloud"></i></span>
                    </li>
                    <li class="nav-item">
                        <span className="text-dark cursor-pointer" onClick={()=>this.onClickLink('/metric-details')}><i class="fas fa-th"></i></span>
                    </li>
                </ul>
            </div>
        )
    }
}
export default withRouter(Sidebar); 