import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { FILE_INFO, UPLOAD_DATA } from '../../redux/constants'
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { getFilesInfo, saveUploadedFile } from '../../redux/actions/dashboardActions'
class UploadPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedFile: {},
            defaultColDef: {
                enableRowGroup: true,
                enablePivot: true,
                enableValue: true,
                sortable: true,
                resizable: false,
                filter: false,
                inputFile: undefined
            },
            paginationPageSize: 10,
            rowSelection: "multiple",
            rowGroupPanelShow: "always",
            pivotPanelShow: "always",
            cacheBlockSize: 10,
            columnDefs: [{
                headerName: "Upload Date Time", field: "uploadTime"
            }, {
                headerName: "File Name", field: "fileName"
            }, {
                headerName: "Records", field: "records"
            },
            {
                headerName: "Download", field: "fileData", width: 220,
                cellRendererFramework: (params) => {
                    return (
                        <div>
                            <button type="button" className="btn btn-sm mr-2 btn-pdf"><a href={`${params.value}`} download="file.pdf">PDF</a></button>
                            <button type="button" className="btn btn-sm ml-2" onClick={this.downloadCSVFile}>CSV</button>
                        </div>
                    )
                }
            }
            ],
            rowData: []
        }
        this.handleClick = this.handleClick.bind(this)
        this.onFileUpload = this.onFileUpload.bind(this);
        this.downloadCSVFile = this.downloadCSVFile.bind(this);
    }
    downloadCSVFile(data) {
        alert(data)
    }
    componentWillMount() {
        this.props.getFilesInfo()
    }
    componentWillReceiveProps(newPros) {
        if (newPros.filesInfo) {
            this.setState({ rowData: newPros.filesInfo })
        }
    }
    handleClick(data) {
        console.log(data)
    }
    onFileUpload(event) {
        let file = event.target.files[0];
        let fd = new FormData();
        fd.append('file', file);
        this.setState({ selectedFile: file,fd:fd })
        // var reader = new FileReader();
        // reader.onload = (event) => {
        //     console.log(event)
        //     let obj = {
        //         uploadTime: new Date(), fileName: file.name, records: Math.floor(Math.random() * 200), "person": "person", fileData: event.target.result
        //     }
        //     console.log(event.target.result)

        //     this.setState({ selectedFile: obj })
        // };

        // reader.readAsDataURL(file);
    }
    onClickUpload=() =>{
        this.props.saveUploadedFile(this.state.fd,(res)=>{
            this.setState({ rowData: [...this.state.rowData, this.state.selectedFile] })
            this.fileInput.value = ""
        })
        // this.props.history.push('/dashboard')
    }

    render() {
        console.log(this.state.rowData)
        return (
            <div className="pl-2">
                <form>
                    <div className="mt-2">
                        Upload your PDF file
                    </div>
                    <div class="form-group mt-2">
                        <label for="exampleInputEmail1" className="col-md-2 p-0"><b>PDF File</b></label>
                        <input type="file" onChange={this.onFileUpload} ref={ref => this.fileInput = ref} />
                        <span className="col-md-3">
                            <button type="button" class="btn btn-primary btn-sm mr-2" onClick={this.onClickUpload}><i class="fas fa-cloud"></i> Upload</button>
                        </span>
                    </div>

                </form>
                <div style={{ width: "62%", height: "100%" }}>
                    <div
                        id="myGrid"
                        style={{
                            height: "400px",
                            width: "100%"
                        }}
                        className="ag-theme-balham"
                    >
                        <AgGridReact
                            modules={this.state.modules}
                            columnDefs={this.state.columnDefs}
                            autoGroupColumnDef={this.state.autoGroupColumnDef}
                            defaultColDef={this.state.defaultColDef}
                            suppressRowClickSelection={true}
                            groupSelectsChildren={true}
                            debug={true}
                            rowSelection={this.state.rowSelection}
                            rowGroupPanelShow={this.state.rowGroupPanelShow}
                            pivotPanelShow={this.state.pivotPanelShow}
                            enableRangeSelection={true}
                            pagination={true}
                            onGridReady={this.onGridReady}
                            rowData={this.state.rowData}
                            cacheBlockSize={this.state.cacheBlockSize}
                            paginationPageSize={this.state.paginationPageSize}
                        />
                    </div>
                </div>


            </div>
        )
    }
}
function mapStateToProps(state) {
    console.log(state)
    return {
        filesInfo: state.filesInfo
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getFilesInfo: getFilesInfo,
        saveUploadedFile: saveUploadedFile
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UploadPage);
