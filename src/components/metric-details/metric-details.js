import React, { Component } from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { FILE_INFO } from '../../redux/constants';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { getFilesInfo,getFileInfoByFile } from '../../redux/actions/dashboardActions'
class MetricDetails extends Component {
    constructor(props) {
        super(props)
        this.state = {
            filterBy:'',
            columns:[
                {id:1,name:"Badge ID",columnName:'Hotstamp'},
                {id:2,name:"Location",columnName:'Location'},
                {id:3,name:"Status",columnName:'Description'},
                {id:4,name:"Person",columnName:'who'},
            ],
            filterValueList: [
               
            ],
            defaultColDef: {
                enableRowGroup: true,
                enablePivot: true,
                enableValue: true,
                sortable: false,
                resizable: false,
                filter: false
            },
            paginationPageSize: 10,
            rowSelection: "multiple",
            rowGroupPanelShow: "always",
            pivotPanelShow: "always",
            cacheBlockSize: 10,
            columnDefs: [{
                headerName: "Badge ID", field: "Hotstamp"
            }, {
                headerName: "Datetime", field: "Date/Time", width: 240
            }, {
                headerName: "Location", field: "Location"
            }, {
                headerName: "Status", field: "Description", width: 250
            }, {
                headerName: "Person", field: "who", width: 250
            }
            //  {
            //     headerName: "Download", field: "fileData",
            //     cellRendererFramework: (params) => {
            //         return <a href={`${params.value}`} download="file.pdf"><i class="fas fa-download"></i></a>
            //     }
            // }
            ],
            rowData: []
        }
        this.handleFilterBy = this.handleFilterBy.bind(this);
        this.applyFilters = this.applyFilters.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    componentWillMount() {
        this.props.getFilesInfo()
        
        this.props.getFileInfoByFile('sample',(data) => {
            this.setState({ rowData: data })
        });
    }
    handleFilterBy(event){
        this.setState({filterBy:event.target.value})
        if(event.target.value==="Hotstamp"){
            let values = this.props.fileInfoByFile.map((item)=>item.Hotstamp)    
            this.setState({filterValueList:values})
        }else if(event.target.value==="Location"){
            let values = this.props.fileInfoByFile.map((item)=>item.Location)    
            this.setState({filterValueList:values})
        }else if(event.target.value==="Description"){
            let values = this.props.fileInfoByFile.map((item)=>item.Description)    
            this.setState({filterValueList:values})
        }else{
            let values = this.props.fileInfoByFile.map((item)=>item.who)    
            this.setState({filterValueList:values})
        }
    }
    applyFilters (){
        let values=[]
        if(this.state.filterBy==='Hotstamp'){
             values = this.props.fileInfoByFile.map((item)=>{
                if(item[this.state.filterBy]===parseInt(this.state.selectedFilterValue)){
                    return item
                }
            })
        }else{
            values = this.props.fileInfoByFile.map((item)=>{
                if(item[this.state.filterBy]===this.state.selectedFilterValue){
                    return item
                }   
            })
        }
        this.gridApi.setRowData(values)
        // this.setState({fileInfoList:values})
       
    }
    handleChange(evt){
        this.setState({selectedFilterValue:evt.target.value})
    }
    onFileSelect=(event)=>{
        if(event.target.value.split('.')[0]==='sample'){
            this.props.getFileInfoByFile('sample',(data) => {
                this.setState({ rowData: data })
            });
        }else{
            this.props.getFileInfoByFile('file_2',(data) => {
                this.setState({ rowData: data })
            });
        }
       

    }
    onGridReady=(params)=>{
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }
    render() {
        console.log("Rendering Metrics");
        return (
            <div className="pl-2">
                <form className="pt-3">
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"><b>Metric Details</b></label>
                        <div class="col-sm-10">
                            <select class="form-control col-md-4 form-control-sm" onChange={(event)=>this.onFileSelect(event)}>
                                <option>Files</option>
                                {
                                    this.props.filesInfo.map((file, index) => {
                                        return (
                                            <option key={index} id={file.id}>{file.fileName}</option>
                                        )
                                    }
                                    )
                                }
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div className="col-md-12">
                            <div className="row">
                                <div className="col-md-2">
                                    <div className="row">
                                        <label className="col-md-3 pr-0"><b>From</b></label>
                                        <div className="col-md-9"><input type="date"className="col-md-12" /></div>
                                    </div>
                                </div>
                                <div className="col-md-2">
                                    <div className="row">
                                        <label className="col-md-3 p-0 text-right"><b>To</b></label>
                                        <div className="col-md-9"><input type="date" className="col-md-12"/></div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="row">
                                        <label className="col-md-6 text-center"><b>Filter By</b></label>
                                        <div className="col-md-6">
                                            <select class="form-control col-md-12 form-control-sm"    onChange={(event)=>this.handleFilterBy(event)}>
                                                <option>Select an option</option>
                                                {
                                                    this.state.columns.map((column, index) => {
                                                        return (
                                                            <option key={index}  value={column.columnName} >{column.name}</option>
                                                        )
                                                    }

                                                    )
                                                }
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                <div className="row">
                                <label className="col-md-6 text-center"><b>Filter Value</b></label>
                                        <div className="col-md-6">
                                            <select class="form-control col-md-12 form-control-sm" value={this.state.selectedFilterValue} onChange={this.handleChange}>
                                                <option>Select an option</option>
                                                {
                                                    this.state.filterValueList.map((file, index) => {
                                                        return (
                                                            <option key={index} >{file}</option>
                                                        )
                                                    }

                                                    )
                                                }
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-1"><span onClick={this.applyFilters} className="cursor-pointer"><i class="fas fa-filter"></i></span></div>
                            </div>
                        </div>
                    </div>
                </form>
                <div style={{ width: "86%", height: "100%" }}>
                    <div
                        id="myGrid"
                        style={{
                            height: "400px",
                            width: "100%"
                        }}
                        className="ag-theme-balham"
                    >
                        <AgGridReact
                            modules={this.state.modules}
                            columnDefs={this.state.columnDefs}
                            autoGroupColumnDef={this.state.autoGroupColumnDef}
                            defaultColDef={this.state.defaultColDef}
                            suppressRowClickSelection={true}
                            groupSelectsChildren={true}
                            debug={true}
                            rowSelection={this.state.rowSelection}
                            rowGroupPanelShow={this.state.rowGroupPanelShow}
                            pivotPanelShow={this.state.pivotPanelShow}
                            enableRangeSelection={true}
                            pagination={true}
                            onGridReady={this.onGridReady}
                            rowData={this.state.rowData}
                            cacheBlockSize={this.state.cacheBlockSize}
                            paginationPageSize={this.state.paginationPageSize}
                        />
                    </div>
                </div>


            </div>
        )
    }
}
function mapStateToProps(state) {
    console.log(state)
    return {
        filesInfo: state.filesInfo,
        fileInfoByFile:state.fileInfoByFile
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getFilesInfo: getFilesInfo,
        getFileInfoByFile:getFileInfoByFile

    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MetricDetails
);