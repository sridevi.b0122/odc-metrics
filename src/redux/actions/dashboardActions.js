import {FILE_INFO} from '../../redux/constants';
import axios from 'axios';
export function getFilesInfo() {
    return function (dispatch){
      return axios.get('/uploadedFiles').then((res)=>{
        dispatch( {
          "type": 'UPLOAD_FILES_INFO',
          "payload": res.data
      })
      }) .catch(function (error) {
        alert(error)
        console.log(error);
      })
    }
   }
   export function getFileInfoByFile(fileName,cb) {
    return function (dispatch){
      return axios.get(`http://localhost:3001/${fileName}`).then((res)=>{
        cb(res.data)
        dispatch( {
          "type": 'FILE_INFO_BY_FILE',
          "payload": res.data
      })
      }) .catch(function (error) {
        alert(error)
        console.log(error);
      })
    }
   }
export function saveUploadedFile(data,cb){
  return function (dispatch){
   
    return axios.post('http://localhost:3001/uploadedFiles',data, {headers: {
      'Accept': 'application/json',
        'Content-Type': 'application/json'}
  }).then((res)=>{
      cb(res.data)
      dispatch( {
        "type": 'INSERT_FILE',
        "payload": data
    })
    }) .catch(function (error) {
      alert(error)
      console.log(error);
    })
  }
}
export function downloadFile(data,cb){
  return function (dispatch){
   
    return axios.get('http://localhost:3001/uploadedFiles',data, {headers: {
      'Accept': 'application/json',
        'Content-Type': 'application/json'}
  }).then((res)=>{
      cb(res.data)
      dispatch( {
        "type": 'DOWNLOAD_FILE',
        "payload": data
    })
    }) .catch(function (error) {
      alert(error)
      console.log(error);
    })
  }
}
  
