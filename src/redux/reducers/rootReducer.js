import { combineReducers } from 'redux';
import {uploadReducer,fileReducer} from './dashboardReducer'


export default combineReducers({
  filesInfo:uploadReducer,
  fileInfoByFile:fileReducer
})