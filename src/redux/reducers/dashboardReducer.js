
export function uploadReducer(state = [], action) {
    switch (action.type) {
        case 'UPLOAD_FILES_INFO':
            return action.payload
        case 'INSERT_FILE':
            return [...state,action.payload]
        default:
            return state;
    }
}
export function fileReducer(state = [], action) {
    switch (action.type) {
        case 'FILE_INFO_BY_FILE':
            return action.payload
        default:
            return state;
    }
}
export function downloadFileReducer(state = [], action) {
    switch (action.type) {
        case 'DOWNLOAD_FILE':
            return action.payload
        default:
            return state;
    }
}